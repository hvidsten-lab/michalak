---
title: "AcGGM pigtrial"
author: "Torgeir R Hvidsten"
date: "3/3/2019"
output:
  html_document:
    toc: true
    toc_float: true
    theme: yeti
    code_folding: hide
    number_sections: true
editor_options: 
  chunk_output_type: console
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(tidyverse)
library(gplots)
library("RColorBrewer")
library(DT)
```

# Import data

Removed a few proteins with zero variance and that are present in only one sample. Then merge data into protein groups (with identical expression profiles).

```{r data, warning=FALSE, warning=FALSE, message=FALSE, error=FALSE}

compute = FALSE

if (compute) {
  files = list.files("data/")
  
  data <- tibble()
  for(i in 1:length(files)) {
    tmp <- read_delim(paste0("data/", files[i]), delim = "\t", progress = F)
    tmp <- cbind(tmp[,1], mutate_if(tmp[,c(2:9)], is.character, as.double), tmp[,c(19,21:23)])
    mags <- unlist(lapply(strsplit(tmp$IDcomb, "_"), function(x) {x[2]}))
    tmp$MAG <- mags
    tmp <- tmp[,c(1:9,11:14)]
  
    data <- rbind(data,tmp)
  }
  data <- as.data.frame(data)
  rownames(data) <- data$GBprotID
  data <- data[,-1]
  
  annot <- data[,9:12]
  data <- data[,1:8]
  
  data[is.na(data)] <- 0
  
  samples <- colnames(data)
  samples <- gsub("LFQ intensity ", "", samples)
  colnames(data) <- samples
  
  save(data, annot, file = "table.RData")
} else {
  load("table.RData")
}

print(paste0("Proteins x Samples: ", paste0(dim(data), collapse = " x ")))

# Filter variance
row_sd <- apply(data, 1, sd)
data <- data[row_sd > 0,]
annot <- annot[row_sd > 0,]
print(paste0("Filter: Zero variance: Proteins x Samples: ", paste0(dim(data), collapse = " x ")))

# At least two values
idx <- (rowSums(data > 0)) > 1
data <- data[idx,]
annot <- annot[idx,]
print(paste0("Filter: Single sample expr: Proteins x Samples: ", paste0(dim(data), collapse = " x ")))

# Identify identical proteomics measurements
vals <- apply(data, 1, function(x) paste(x, collapse = "-"))
unique.data <- cbind(data, data.frame(proteins = rownames(data), Val = vals)) %>%
  group_by(Val) %>%
  mutate(ProtGroup = group_indices())

annot$ProtGroup <- unique.data$ProtGroup

unique.data <- unique.data %>%
  group_by(Val) %>%
  slice(1)

unique.data <- as.data.frame(unique.data)
rownames(unique.data) <- unique.data$ProtGroup
unique.data <- unique.data[,1:8]
print(paste0("Protein groups x Samples: ", paste0(dim(unique.data), collapse = " x ")))
```

# PCA/Hierarcical clustering

A PCA seprates Ctr and Mannan pigs. There less variance within the Ctr than within Mannan.

The hierarchical clustering of the heatmap confirms the PCA grouping (see column-tree) and cluster proteins (rows) into five broad clusters:

* Up in some Ctr samples (CtrSome)
* Up in all Ctr samples  (CtrAll)
* Up in most samples (All)
* Up in all Mannan samples (MannanAll)
* Up in all Mannan samples and Ctr4 (MannanAll+Ctr4)

```{r pca, warning=FALSE}

samples <- colnames(unique.data)

# PCA
pc <- prcomp(t(unique.data))
var.expl <- pc$sdev^2 / sum(pc$sdev^2)
var.expl
fig <- ggplot(as.data.frame(pc$x), aes(PC1, PC2, col = samples)) + geom_point() + 
  xlab(paste0("PC1 (", round(var.expl[1], digits=2),")")) +
  ylab(paste0("PC2 (", round(var.expl[2], digits=2),")"))

print(fig)
pdf(file = paste0("PCA.pdf")); print (fig); invisible(dev.off())
  
# ======================

# Hierarchical clustering
if (compute) {
  dist.var <- as.dist(1-cor(unique.data))
  dist.var.tre <- hclust(dist.var, method = "ward.D")
  plot(dist.var.tre, cex = 1)
  
  dist.obs <- as.dist(1-cor(t(unique.data)))
  dist.obs.tre <- hclust(dist.obs, method = "ward.D")
  save(dist.var.tre, dist.obs.tre, file = paste0("trees.RData"))
} else {
  load(paste0("trees.RData"))
}

no.clust <- 5
clusters <- cutree(dist.obs.tre, k=no.clust)
gg_color_hue <- function(n) {
  hues = seq(15, 375, length = n + 1)
  hcl(h = hues, l = 65, c = 100)[1:n]
}
clustercolor <- gg_color_hue(no.clust)

create_heatmap <- function(...) {
  plot_heatmap <- function() heatmap.2(x = as.matrix(unique.data), 
          Rowv = as.dendrogram(dist.obs.tre),
          Colv = as.dendrogram(dist.var.tre),
          trace = "none",
          key = FALSE,
          scale = "row",
          col = colorRampPalette(c("blue","white","red")),
          labRow = c(""),
          labCol = samples,
          cexCol = 1,
          srtCol = 45,
          RowSideColors = clustercolor[factor(clusters)] # Clusters
          )
}

show_heatmap <- create_heatmap()
show_heatmap()

clust.descr <- c("MannanAll","MannanAll+Ctr4","CtrSome","CtrAll","All")
clust.o <- c(3,4,5,1,2)

legend(x = -0.13, y = 1.1, xpd = TRUE, # x = -0.13, y = 1.3
       legend = clust.descr[clust.o],
       col = clustercolor[clust.o],
       pch = 15,
       cex = 1,
       bty = "n" 
    )

pdf(file = paste0("Heatmap.pdf")); show_heatmap(); invisible(dev.off())

```

# EC enrichment analysis

The table shows which EC numbers that are enriched in expression clusters. For example:

- 1.4.1.4	NADP-specific glutamate dehydrogenase	CtrAll	2.6e-06	4.5e-04	31	732	55	2738

means that there are 2738 protein groups in the dataset of which 55 have EC number 1.4.1.4. Of the 732 protein groups in the CtrAll-cluster, 31 have EC number 1.4.1.4. This represents a statistically significant enrichment as seen by the low p-value and FDR correct p-value .

You can input text in the box above each column to filter the results. For example select "CtrAll" in the "Cluster"-column and you will now see the EC-numbers that are most enriched in proteins up-regulated in all Ctrl samples.

```{r enrichment, warning=FALSE}

E <- data.frame()

annot_idx <- match(rownames(unique.data), annot$ProtGroup)

t <- table(annot$EC_number[annot_idx])
t <- t[t > 1]

N <- sum(!is.na(annot$EC_number[annot_idx]))
for (i in 1:length(t)) {

  ec <- names(t)[i]
  ec.name <- annot$product[!is.na(annot$EC_number) & annot$EC_number == ec][1]
  
  m <- sum(!is.na(annot$EC_number[annot_idx]) & annot$EC_number[annot_idx] == ec)
  n <- N-m
  
  for (j in 1:no.clust) {
    
    k <- sum(clusters == j & !is.na(annot$EC_number[annot_idx]))
    
    x <- sum(clusters == j & !is.na(annot$EC_number[annot_idx]) & annot$EC_number[annot_idx] == ec)

    p <- 1
    if (x > 1) {
      p <- phyper(x-1, m, n, k, lower.tail = FALSE)
      if (p < 1) {
        
        if (nrow(E) == 0) {
          E <- rbind(E, c(ec,ec.name,clust.descr[j],p,p,x,k,m,N), stringsAsFactors=FALSE)
          colnames(E) <- c("EC", "EC name", "Cluster", "P-value", "Adj. P-value", "x", "k", "m", "N")
        } else {
          tmp <- data.frame()
          tmp <- rbind(tmp, c(ec,ec.name,clust.descr[j],p,p,x,k,m,N), stringsAsFactors=FALSE)
          colnames(tmp) <- c("EC", "EC name", "Cluster", "P-value", "Adj. P-value", "x", "k", "m", "N")
          E <- rbind(E, tmp, stringsAsFactors=FALSE)
        }
      }
    }
  }
}
E$`P-value` <- as.numeric(E$`P-value`)
E$`Adj. P-value` <- p.adjust(E$`P-value`, method = "BH")
E$x <- as.numeric(E$x)
E$k <- as.numeric(E$k)
E$m <- as.numeric(E$m)
E$N <- as.numeric(E$N)
E$EC <- as.factor(E$EC)
E$Cluster <- as.factor(E$Cluster)

E$`P-value` <- format(E$`P-value`, scientific = TRUE,  digits=2)
E$`Adj. P-value` <- format(E$`Adj. P-value`, scientific = TRUE,  digits=2)

datatable(E, rownames = FALSE, filter = "top", options = list(
  order = list(list(3, 'asc'))))

```

## Explore the proteins in the clusters - protein group level

E.g. to find the 31 protein groups behind the following enrichment (from the above table):

- 1.4.1.4	NADP-specific glutamate dehydrogenase	CtrAll	2.6e-06	4.5e-04	31	732	55	2738

simply filter the EC-column by "1.4.1.4" and the Cluster-column by "CtrAll".

```{r fun_table, warning=FALSE}

annot2 <- cbind(annot[annot_idx,], clust.descr[clusters])
colnames(annot2)[6] <- "cluster"

annot2$ProtGroup <- as.factor(annot2$ProtGroup)
annot2$EC_number <- as.factor(annot2$EC_number)
annot2$cluster <- as.factor(annot2$cluster)

datatable(annot2, rownames = FALSE, filter = "top")

```

## Explore the proteins in the clusters - protein level

Here instead of showing one protein group per row, we show one protein per row. Proteins from the same protein group have identical expression profiles but matches different MAGs.

```{r fun_table2, warning=FALSE}

idx <- match(annot$ProtGroup, annot2$ProtGroup)
clusters2 <- annot2$cluster[idx]

annot2 <- cbind(rownames(annot), annot, clusters2)
colnames(annot2)[1] <- "GBprotID"
colnames(annot2)[7] <- "cluster"

annot2$ProtGroup <- as.factor(annot2$ProtGroup)

datatable(annot2, rownames = FALSE, filter = "top")

```

# MAG enrichment analysis

## MAG - Cluster enrichment analysis

The table shows which MAGs that are enriched in expression clusters. That is, is there any MAG that contribute with more proteins in a cluster than what we would expect by chance. For example:

- bin053	MannanAll	3.8e-36	2.5e-33	156	4375	200	12535

means that there are 12535 proteins in the dataset of which 200 is in bin053. Of the 4375 proteins in the MannanAll-cluster, 156 are in bin053. This represents a massive enrichment as seen by the low p-value and adjusted p-value.

```{r mag_enrichment, warning=FALSE}

E <- data.frame()

mags <- annot$MAG
t <- table(mags)
t <- t[t > 1]
mag.s <- names(t)

N <- nrow(data)
for (i in 1:length(t)) {

  mag <- mag.s[i]

  mag.genes <- rownames(annot)[mags == mag]
  
  m <- length(mag.genes)
  n <- N-m
  
  for (j in 1:no.clust) {
    
    k <- sum(clusters2 == clust.descr[j])
    
    x <- sum(clusters2 == clust.descr[j] & mags == mag)

    p <- 1
    if (x > 1) {
      p <- phyper(x-1, m, n, k, lower.tail = FALSE)
      if (p < 1) {
        
        if (nrow(E) == 0) {
          E <- rbind(E, c(mag,clust.descr[j],p,p,x,k,m,N), stringsAsFactors=FALSE)
          colnames(E) <- c("MAG", "Cluster", "P-value", "Adj. P-value", "x", "k", "m", "N")
        } else {
          tmp <- data.frame()
          tmp <- rbind(tmp, c(mag,clust.descr[j],p,p,x,k,m,N), stringsAsFactors=FALSE)
          colnames(tmp) <- c("MAG", "Cluster", "P-value", "Adj. P-value", "x", "k", "m", "N")
          E <- rbind(E, tmp, stringsAsFactors=FALSE)
        }
      }
    }
  }
}
E$`P-value` <- as.numeric(E$`P-value`)
E$`Adj. P-value` <- p.adjust(E$`P-value`, method = "BH")
E$x <- as.numeric(E$x)
E$k <- as.numeric(E$k)
E$m <- as.numeric(E$m)
E$N <- as.numeric(E$N)
E$MAG <- as.factor(E$MAG)
E$Cluster <- as.factor(E$Cluster)

E <- E[E$`Adj. P-value` <= 0.05,]

E$`P-value` <- format(E$`P-value`, scientific = TRUE,  digits=2)
E$`Adj. P-value` <- format(E$`Adj. P-value`, scientific = TRUE,  digits=2)

datatable(E, rownames = FALSE, filter = "top", options = list(
  order = list(list(3, 'asc'))))

```

## MAG - EC-number enrichment analysis

The table shows which MAGs that are enriched in EC numbers. That is, is there any MAG that contribute with more proteins to an EC-number than what we would expect by chance. For example:

- bin097	3.5.1.5	Urease subunit beta	1.8e-06	1.3e-04	3	6	57	12535

means that there are 12535 proteins in the dataset of which 57 is in bin097. Of the 6 proteins with EC-number 3.5.1.5, 3 are in bin097. This represents an enrichment as seen by the low p-value and adjusted p-value.

```{r mag_enrichment2, warning=FALSE}

E <- data.frame()

mags <- annot$MAG
t <- table(mags)
t <- t[t > 1]
mag.s <- names(t)

N <- nrow(data)
for (i in 1:length(t)) {

  mag <- mag.s[i]

  mag.genes <- rownames(annot)[mags == mag]
  
  m <- length(mag.genes)
  n <- N-m
  
  tt <- table(annot$EC_number)
  tt <- tt[tt > 1]

  for (j in 1:length(tt)) {
    
    ec <- names(tt)[j]
    ec.name <- annot$product[!is.na(annot$EC_number) & annot$EC_number == ec][1]
    
    k <- sum(!is.na(annot$EC_number) & annot$EC_number == ec)
    
    x <- sum(!is.na(annot$EC_number) & annot$EC_number == ec & mags == mag)

    p <- 1
    if (x > 1) {
      p <- phyper(x-1, m, n, k, lower.tail = FALSE)
      if (p < 1) {
        
        if (nrow(E) == 0) {
          E <- rbind(E, c(mag,ec,ec.name,p,p,x,k,m,N), stringsAsFactors=FALSE)
          colnames(E) <- c("MAG", "EC", "EC name", "P-value", "Adj. P-value", "x", "k", "m", "N")
        } else {
          tmp <- data.frame()
          tmp <- rbind(tmp, c(mag,ec,ec.name,p,p,x,k,m,N), stringsAsFactors=FALSE)
          colnames(tmp) <- c("MAG", "EC", "EC name", "P-value", "Adj. P-value", "x", "k", "m", "N")
          E <- rbind(E, tmp, stringsAsFactors=FALSE)
        }
      }
    }
  }
}
E$`P-value` <- as.numeric(E$`P-value`)
E$`Adj. P-value` <- p.adjust(E$`P-value`, method = "BH")
E$x <- as.numeric(E$x)
E$k <- as.numeric(E$k)
E$m <- as.numeric(E$m)
E$N <- as.numeric(E$N)
E$MAG <- as.factor(E$MAG)
E$EC <- as.factor(E$EC)

E <- E[E$`Adj. P-value` <= 0.05,]

E$`P-value` <- format(E$`P-value`, scientific = TRUE,  digits=2)
E$`Adj. P-value` <- format(E$`Adj. P-value`, scientific = TRUE,  digits=2)

datatable(E, rownames = FALSE, filter = "top", options = list(
  order = list(list(3, 'asc'))))

```
